Sutty PostSRSd
==============

SRS is a technique for e-mail forwarding that doesn't break SPF
checks by rewriting the sender.  This prevents other e-mail servers to
bounce our e-mail or catalog it as spam.

The secret is distributed because it's checked during bounces and
a bounce can arrive between container restarts and maybe to any node.

Role Variables
--------------

Needs a shared `secret` between all instances, a `domain` from which
addresses are rewritten, and Docker `network`.

* `secret`: Shared secret between instances
* `domain`: Main domain
* `network`: Docker network
* `data`: Shared storage outside the container
* `dest`: Secret location (default: "/etc/postsrsd.secret")

Example Playbook
----------------

```yaml
- name: "postsrsd"
  include_role:
    name: "sutty-postsrsd"
  vars:
    network: "{{ docker_network }}"
    secret: "{{ postsrsd_secret }}"
    domain: "{{ sutty }}"
```

License
-------

MIT-Antifa
